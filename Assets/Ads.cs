﻿using UnityEngine;
using UnityEngine.Advertisements;

public class Ads : MonoBehaviour
{
    public static Ads instance;

    private void Start()
    {
        instance = this;
        Advertisement.Initialize("3939831");
    }
    public void ShowAds()
    {
        if (PlayerPrefs.GetInt("DeathCounter") >= 4)
        {
            if (Advertisement.IsReady())
            {
                Advertisement.Show(("video"));
                Debug.Log("Реклама");
            }
            PlayerPrefs.SetInt("DeathCounter", 0);
        }
    }
}