﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCounter : MonoBehaviour
{
    public int Score;
    public static ScoreCounter instance;
    private void Start()
    {
        instance = this;
    }
    void OnTriggerExit(Collider other)
    {
        if(other.tag == "Score")
        {
            Score++;
        }
    }
}