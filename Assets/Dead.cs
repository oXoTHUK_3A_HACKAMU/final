﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dead : MonoBehaviour
{
    public Image deadScreen;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Player")
        {
            Time.timeScale = 0;
            int count = PlayerPrefs.GetInt("DeathCounter");
            count++;
            PlayerPrefs.SetInt("DeathCounter", count);
            deadScreen.gameObject.SetActive(true);
            Debug.Log(PlayerPrefs.GetInt("DeathCounter"));
            GAManager.instance.Progress(ScoreCounter.instance.Score);
            Ads.instance.ShowAds();
            FaceBookManager.instance.Score(ScoreCounter.instance.Score);
        }
    }
}