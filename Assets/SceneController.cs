﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public void ChangeScene(int index)
    {
        SceneManager.LoadScene(index);
        PlayerPrefs.SetInt("DeathCounter", 0);
    }

    public void Reatart(int index)
    {
        SceneManager.LoadScene(index);
        Physics.gravity = new Vector3(0, -10, 0);
        Time.timeScale = 1;
    }
}
