﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class GAManager : MonoBehaviour
{
    public static GAManager instance;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
    }
    private void Start()
    {
        GameAnalytics.Initialize();
    }

    public void Progress(int Score)
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, Score.ToString());
        Debug.Log("Score : " + ScoreCounter.instance.Score);
    } 
}