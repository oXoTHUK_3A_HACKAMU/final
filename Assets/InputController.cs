﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    bool pressing;
    void ChangeGravity(float index)
    {
        Physics.gravity = new Vector3(0, index, 0);
    }
    private void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                if (!pressing)
                {
                    pressing = true;
                    ChangeGravity(10);
                }
                else
                {
                    pressing = false;
                    ChangeGravity(-10);
                }
            }
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        { 
                if (!pressing)
                {
                    pressing = true;
                    ChangeGravity(10);
                }
                else
                {
                    pressing = false;
                    ChangeGravity(-10);
                }
        }
#endif
    }
}
