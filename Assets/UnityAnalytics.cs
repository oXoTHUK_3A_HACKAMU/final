﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class UnityAnalytics : MonoBehaviour
{
    public static UnityAnalytics instance;
    private void Start()
    {
        instance = this;
    }
    public void PlayerScore()
    {
        Analytics.CustomEvent("Score : " + ScoreCounter.instance.Score.ToString());
    }
}
